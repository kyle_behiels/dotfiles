#!/usr/bin/env bash

BAT_EMP=":";
BAT_LOW=":";
BAT_MED=":";
BAT_HIG=":";
BAT_FUL=":";


while [ true ] 
do
	# Battery stuff start

	BATTERY_POWER=$( upower -i `upower -e | grep 'BAT'` | grep percentage | cut -d : -f 2 | cut -d % -f 1 | tr -d '[:space:]' );
	
	LOG_BAT=$BAT_FUL;

	if [ $BATTERY_POWER -lt 100 ]
	then
		LOG_BAT=$BAT_HIG;
	fi
	if [ $BATTERY_POWER -lt 75 ]
	then
		LOG_BAT=$BAT_MED;
	fi
	if [ $BATTERY_POWER -lt 50 ]
	then
		LOG_BAT=$BAT_LOW;
	fi
	if [ $BATTERY_POWER -lt 25 ]
	then
		LOG_BAT=$BAT_EMP;
	fi
	
	# Battery stuff end

	# Network stuff start

	NET_NAME=$( echo $( iwconfig wlp2s0 | grep ESSID | cut -d '"' -f 2 ) );

	NET_STRENGTH=$( echo $( iwconfig wlp2s0 | grep Link\ Quality | cut -d = -f 2 | cut -d " " -f 1 ) )

	#NET_RATIO=$(( NET_STRENGTH_DIVIDEND / NET_STRENGTH_DIVISOR ));
	
	echo ": $USER | $LOG_BAT $BATTERY_POWER% | : $NET_NAME:$NET_STRENGTH | $( date '+%r' )";
	sleep 10;
done
